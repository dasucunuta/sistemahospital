package edu.istdab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HospitalAppBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(HospitalAppBackendApplication.class, args);
	}

}
