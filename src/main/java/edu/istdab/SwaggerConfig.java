package edu.istdab;

import java.util.ArrayList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.service.Contact;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	public static final Contact DEFA_CONTACT = new Contact("Omer Benitez","https://www.omer.com",
			"omerbenitez212000@gamil.com");
	public static final ApiInfo DEF_API_INFO = new ApiInfo("Sistema Hospitalario Api Documentation", "Sistema Hospitalario Api Documentation",
			"1.0", "Premium", DEFA_CONTACT, "Apache 2.0", "https://www.omer.com",new ArrayList<VendorExtension>());
	
	@Bean
	public Docket api(){
		return new Docket(DocumentationType.SWAGGER_2).apiInfo(DEF_API_INFO);
	}
}
