package edu.istdab.service;


import edu.istdab.dto.ConsultaListaExamenDTO;
import edu.istdab.model.Consulta;

public interface IConsultaService extends ICRUD<Consulta> {

	Consulta registrarTransaccional(ConsultaListaExamenDTO dto);
}
