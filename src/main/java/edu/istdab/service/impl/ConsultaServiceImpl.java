package edu.istdab.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.istdab.dto.ConsultaListaExamenDTO;
import edu.istdab.model.Consulta;
import edu.istdab.repo.IConsultaExamenRepo;
import edu.istdab.repo.IConsultaRepo;
import edu.istdab.service.IConsultaService;

@Service
public class ConsultaServiceImpl implements IConsultaService{

	@Autowired
	private IConsultaRepo repo;
	
	@Autowired
	private IConsultaExamenRepo ceRepo;
	
	//Solo registra Consulta
	@Override
	public Consulta registrar(Consulta consulta) {
		// TODO Auto-generated method stub
		consulta.getDetalleConsulta().forEach(det -> {
			det.setConsulta(consulta);
		});
		return repo.save(consulta);
	}
	
	//Registra consulta y examen por eso es Transaccional
	@Transactional
	@Override
	public Consulta registrarTransaccional(ConsultaListaExamenDTO dto) {
		// TODO Auto-generated method stub
		dto.getConsulta().getDetalleConsulta().forEach(dc ->{
			dc.setConsulta(dto.getConsulta());
		});
		repo.save(dto.getConsulta());
		
		dto.getLstExamen().forEach(ex ->{
			ceRepo.registrar(dto.getConsulta().getIdConsulta(),ex.getIdExamen() );
		});
		
		return dto.getConsulta();
	}
	
	@Override
	public Consulta modificar(Consulta obj) {
		// TODO Auto-generated method stub
		return repo.save(obj);
	}

	@Override
	public List<Consulta> listar() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}

	@Override
	public Consulta leerPorId(Integer id) {
		// TODO Auto-generated method stub
		Optional<Consulta> con=repo.findById(id);
		return con.isPresent() ? con.get() : new Consulta();
	}

	@Override
	public boolean eliminar(Integer id) {
		// TODO Auto-generated method stub
		repo.deleteById(id);
		return true;
	}
	
}
