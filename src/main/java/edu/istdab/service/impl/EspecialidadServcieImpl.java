package edu.istdab.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.istdab.model.Especialidad;
import edu.istdab.repo.IEspecialidadRepo;
import edu.istdab.service.IEspecialidadService;

@Service
public class EspecialidadServcieImpl implements IEspecialidadService{

	@Autowired
	private IEspecialidadRepo repo;
	
	@Override
	public Especialidad registrar(Especialidad obj) {
		// TODO Auto-generated method stub
		return repo.save(obj);
	}

	@Override
	public Especialidad modificar(Especialidad obj) {
		// TODO Auto-generated method stub
		return repo.save(obj);
	}

	@Override
	public List<Especialidad> listar() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}

	@Override
	public Especialidad leerPorId(Integer id) {
		// TODO Auto-generated method stub
		Optional<Especialidad> esp=repo.findById(id);
		return esp.isPresent() ? esp.get() : new Especialidad();
	}

	@Override
	public boolean eliminar(Integer id) {
		// TODO Auto-generated method stub
		repo.deleteById(id);
		return true;
	}

}
