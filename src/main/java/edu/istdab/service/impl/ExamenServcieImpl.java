package edu.istdab.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.istdab.model.Examen;
import edu.istdab.repo.IExamenRepo;
import edu.istdab.service.IExamenService;

@Service
public class ExamenServcieImpl implements IExamenService{

	@Autowired
	private IExamenRepo repo;

	@Override
	public List<Examen> listar() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}
	
	@Override
	public boolean eliminar(Integer id) {
		// TODO Auto-generated method stub
		repo.deleteById(id);
		return true;
	}

	@Override
	public Examen registrar(Examen obj) {
		// TODO Auto-generated method stub
		return repo.save(obj);
	}

	@Override
	public Examen modificar(Examen obj) {
		// TODO Auto-generated method stub
		return repo.save(obj);
	}

	@Override
	public Examen leerPorId(Integer id) {
		// TODO Auto-generated method stub
		Optional<Examen> esp=repo.findById(id);
		return esp.isPresent() ? esp.get() : new Examen();
	}

}
