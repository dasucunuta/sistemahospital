package edu.istdab.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.istdab.model.Medico;
import edu.istdab.repo.IMedicoRepo;
import edu.istdab.service.IMedicoService;

@Service
public class MedicoServiceImpl implements IMedicoService {

	@Autowired
    private IMedicoRepo repo;
	
	
	@Override
	public Medico registrar(Medico medico) {
		// TODO Auto-generated method stub
		return repo.save(medico);
	}

	@Override
	public Medico modificar(Medico medico) {
		// TODO Auto-generated method stub
		return repo.save(medico);
	}

	@Override
	public List<Medico> listar() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}

	@Override
	public Medico leerPorId(Integer id) {
		// TODO Auto-generated method stub
		Optional<Medico> op=repo.findById(id);
		return op.isPresent() ? op.get() : new Medico();
	}

	@Override
	public boolean eliminar(Integer id) {
		// TODO Auto-generated method stub
		repo.deleteById(id);
		return true;
	}

	
}
