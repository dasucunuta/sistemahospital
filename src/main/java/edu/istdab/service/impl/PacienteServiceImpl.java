package edu.istdab.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.istdab.model.Paciente;
import edu.istdab.repo.IPacienteRepo;
import edu.istdab.service.IPacienteService;

@Service
public class PacienteServiceImpl implements IPacienteService {

	@Autowired
	private IPacienteRepo repo;
	
	@Override
	public Paciente registrar(Paciente paciente) {
		// TODO Auto-generated method stub
		return repo.save(paciente);
	}

	@Override
	public Paciente modificar(Paciente paciente) {
		// TODO Auto-generated method stub
		return repo.save(paciente); 
	}

	@Override
	public List<Paciente> listar() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}

	@Override
	public Paciente leerPorId(Integer id) {
		// TODO Auto-generated method stub
		Optional<Paciente> op= repo.findById(id);
		return op.isPresent() ? op.get() : new Paciente();
	}

	@Override
	public boolean eliminar(Integer id) {
		// TODO Auto-generated method stub
		repo.deleteById(id);
		return true;
	}

}
