package edu.istdab.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "persona")
@Inheritance(strategy = InheritanceType.JOINED)
public class Persona {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idPersona;
	
	@ApiModelProperty(notes = "El nombre debe tener minimo 3 y maximo 70 caracteres")
	@Size(min = 3,max = 70,message = "El nombre debe tener minimo 3 y maximo 70 caracteres")
	@Column(name = "nombres", nullable = false, length = 70)
	private String nombres;
	
	@ApiModelProperty(notes = "El apellido debe tener minimo 3 y maximo 70 caracteres")
	@Size(min = 3,max = 70,message = "El apellido debe tener minimo 3 y maximo 70 caracteres")
	@Column(name = "apellidos", nullable = false, length = 70)
	private String apellidos;
	
	@ApiModelProperty(notes =  "El nombre debe tener minimo 10 y maximo 10 caracteres")
	@Size(min = 10,max = 10,message = "El nombre debe tener minimo 10 y maximo 10 caracteres")
	@Column(name = "cedula", nullable = false, length = 10)
	private String cedula;
	
	@ApiModelProperty(notes =  "El telefono debe tener minimo 6 y maximo 10 caracteres")
	@Size(min = 6,max = 10,message = "El telefono debe tener minimo 6 y maximo 10 caracteres")
	@Column(name = "telefono", nullable = false, length = 10)
	private String telefono;
	
	@ApiModelProperty(notes = "La direccion debe tener minimo 3 y maximo 200 caracteres")
	@Size(min = 3,max = 200,message = "La direccion debe tener minimo 3 y maximo 200 caracteres")
	@Column(name = "direccion", nullable = false, length = 200)
	private String direccion;
	
	@Email
	@Column(name = "email", nullable = false, length = 70)
	private String email;
	
	@Column(name = "edad", nullable = false, length = 2)
	private int edad;
	
	@ApiModelProperty(notes = "El sexo de la persona tiene que tener minimo y maximo 1 caracteres")
	@Size(min = 1,max = 1,message = "El sexo de la persona tiene que tener minimo y maximo 1 caracteres")
	@Column(name = "sexo", nullable = false, length = 1)
	private String sexo;
	
	@ApiModelProperty(notes =  "El tipo de sangre tiene que tener minimo 1 y maximo 5 caracteres")
	@Size(min = 1,max = 5,message = "El tipo de sangre tiene que tener minimo 1 y maximo 5 caracteres")
	@Column(name = "tipoSangre", nullable = false, length = 5)
	private String tipoSangre;

	//---------------------Metodos Get y Set--------------
	
	
	
	public Integer getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Integer idPersona) {
		this.idPersona = idPersona;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getTipoSangre() {
		return tipoSangre;
	}

	public void setTipoSangre(String tipoSangre) {
		this.tipoSangre = tipoSangre;
	}
	
	
	
	
}
