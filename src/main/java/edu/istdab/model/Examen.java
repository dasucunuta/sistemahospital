package edu.istdab.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Informacion del Examen")
@Entity
@Table(name = "examen")
public class Examen {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idExamen;
	
	@ApiModelProperty(notes = "El nombre del examen tiene que tener minimo 3 caracteres")
	@Size(min = 3,max = 50,message = "El nombre del examen tiene que tener minimo 3 caracteres")
	@Column(name = "nombre", nullable = false, length = 50)
	private String nombre;
	
	@ApiModelProperty(notes =  "La descripcion debe tener minimo 3 caracteres")
	@Size(min = 3,max = 50,message = "La descripcion debe tener minimo 3 caracteres")
	@Column(name = "descripcion", nullable = false, length = 50)
	private String descripcion;

	//-------------Metodos Get y Set-------------
	
	public Integer getIdExamen() {
		return idExamen;
	}

	public void setIdExamen(Integer idExamen) {
		this.idExamen = idExamen;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
}
