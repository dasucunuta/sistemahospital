package edu.istdab.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Informacion de paciente")
@Entity
@Table(name = "paciente")
@PrimaryKeyJoinColumn(name = "id_persona")
public class Paciente extends Persona{

	@ApiModelProperty(notes = "Historia Clinica debe teber minimo 10 caracteres")
	@Column(name = "historia_clinica", nullable = false, length = 10)
	private int historiaClinica;
	
	@ApiModelProperty(notes = "Los nombres de las enfermedades deben tener maximo 50 caracteres")
	@Size(min = 0,max = 50,message = "Los nombres de las enfermedades deben tener maximo 50 caracteres")
	@Column(name = "enfermedades", nullable = true)
	private String enfermedades;

	//-----------Metodos Get y Set-----------------
	
	public int getHistoriaClinica() {
		return historiaClinica;
	}

	public void setHistoriaClinica(int historiaClinica) {
		this.historiaClinica = historiaClinica;
	}

	public String getEnfermedades() {
		return enfermedades;
	}

	public void setEnfermedades(String enfermedades) {
		this.enfermedades = enfermedades;
	}
	
	
}
