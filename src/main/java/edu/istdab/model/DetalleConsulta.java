package edu.istdab.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Informacion del Detalle Consulta")
@Entity
@Table(name = "detalle_consulta")
public class DetalleConsulta {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idDetalle;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "id_consulta", nullable = false, foreignKey = @ForeignKey(name = "FK_DETALLE_CONSULTA"))
	private Consulta consulta;
	
	@ApiModelProperty(notes =  "El diagnostico tiene que tener minimo 4  caracteres y maximo 70 caracteres")
	@Size(min = 4,max = 70,message = "El diagnostico tiene que tener minimo 4  caracteres y maximo 70 caracteres")
	@Column(name = "diagnostico", nullable = false, length = 70)
	private String diagnostico;
	
	@ApiModelProperty(notes = "El tratamiento tiene que tener minimo 4  caracteres y maximo 70 caracteres")
	@Size(min = 4,max = 300,message = "El tratamiento tiene que tener minimo 4  caracteres y maximo 70 caracteres")
	@Column(name = "tratamiento", nullable = false, length = 300)
	private String tratamiento;

	//--------Metodos Get y Set-----------
	
	public Integer getIdDetalle() {
		return idDetalle;
	}

	public void setIdDetalle(Integer idDetalle) {
		this.idDetalle = idDetalle;
	}

	public Consulta getConsulta() {
		return consulta;
	}

	public void setConsulta(Consulta consulta) {
		this.consulta = consulta;
	}

	public String getDiagnostico() {
		return diagnostico;
	}

	public void setDiagnostico(String diagnostico) {
		this.diagnostico = diagnostico;
	}

	public String getTratamiento() {
		return tratamiento;
	}

	public void setTratamiento(String tratamiento) {
		this.tratamiento = tratamiento;
	}
	
	
}
