package edu.istdab.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Informacion del Usuario")
@Entity
@Table(name = "usuario")
public class Usuario {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idUsuario;
	
	@ApiModelProperty(notes ="El nombre del usuario debe tener minimo 3 caracteres")
	@Size(min = 3,max = 20,message = "El nombre del usuario debe tener minimo 3 caracteres")
	@Column(name = "username", nullable = false)
	private String username;
	
	@ApiModelProperty(notes = "Su contraseña debe tener minimo 10 y maximo 20 caracteres")
	@Size(min = 10,max = 20,message = "Su contraseña debe tener minimo 10 y maximo 20 caracteres")
	@Column(name = "password", nullable = false)
	private String password;
	
	@ApiModelProperty(notes = "El estado del usuario tiene que tener minimo y maximo 1 caracter")
	@Size(min = 1,max = 1,message = "El estado del usuario tiene que tener minimo y maximo 1 caracter")
	@Column(name = "estado", nullable = false)
	private String estado;

	//--------Metodos Get y Set----------
	
	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	
}
