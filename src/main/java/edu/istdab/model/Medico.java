package edu.istdab.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Informacion del Medico")
@Entity
@Table(name = "medico")
@PrimaryKeyJoinColumn(name = "id_persona")
public class Medico extends Persona{

	
	@ApiModelProperty(notes ="El consultorio tiene que tener minimo 3 caracteres")
	@Size(min = 3,max = 70,message = "El consultorio tiene que tener minimo 10 caracteres")
	@Column(name = "consultorio", nullable = false, length = 70)
	private String consultorio;
	
	@Column(name = "foto_url", nullable = false, length = 150)
	private String foto_url;

	//------------Metodos Get y Set---------
	
	public String getConsultorio() {
		return consultorio;
	}

	public void setConsultorio(String consultorio) {
		this.consultorio = consultorio;
	}

	public String getFoto_url() {
		return foto_url;
	}

	public void setFoto_url(String foto_url) {
		this.foto_url = foto_url;
	}
	
	
}
