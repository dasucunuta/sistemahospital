package edu.istdab.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.istdab.model.Consulta;


public interface IConsultaRepo extends JpaRepository<Consulta, Integer> {
	

}
