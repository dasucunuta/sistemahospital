package edu.istdab.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.istdab.model.Especialidad;

public interface IEspecialidadRepo extends JpaRepository<Especialidad, Integer> {

}
