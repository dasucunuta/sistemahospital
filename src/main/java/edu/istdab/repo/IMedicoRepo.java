package edu.istdab.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.istdab.model.Medico;


public interface IMedicoRepo extends JpaRepository<Medico, Integer> {
	

}
