package edu.istdab.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.istdab.model.Examen;


public interface IExamenRepo extends JpaRepository<Examen, Integer> {
	

}
