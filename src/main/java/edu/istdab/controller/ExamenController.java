package edu.istdab.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import edu.istdab.exception.ModeloNotFoudException;
import edu.istdab.model.Examen;
import edu.istdab.service.IExamenService;

@RestController
@RequestMapping("/examenes")
public class ExamenController {

	@Autowired
	private IExamenService service;
	
	@GetMapping
	public ResponseEntity<List<Examen>> listar() {
		List<Examen> lista= service.listar();
		return new ResponseEntity<List<Examen>>(lista,HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Examen> listarPorId(@PathVariable("id") Integer id)
	{
		Examen ex= service.leerPorId(id);
		if (ex.getIdExamen()==null) {
			throw new ModeloNotFoudException("ID no encontrado "+id);
		}
		return new ResponseEntity<Examen>(ex, HttpStatus.OK); 
	}
	
	@PostMapping
	public ResponseEntity<Examen> registrar(@RequestBody Examen examen)
	{
		Examen ex =  service.registrar(examen);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(ex.getIdExamen()).toUri();
		return ResponseEntity.created(location).build(); 
	}
	
	@PutMapping
	public ResponseEntity<Examen> modificar(@Valid @RequestBody Examen examen)
	{
		Examen ex =  service.registrar(examen);
		return new ResponseEntity<Examen>(ex, HttpStatus.OK); 
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> eliminar(@PathVariable("id") Integer id)
	{
		Examen ex =  service.leerPorId(id);
		if (ex.getIdExamen()==null) {
			throw new ModeloNotFoudException("ID no encontrado "+id);
		}
		service.eliminar(id);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
	
}
