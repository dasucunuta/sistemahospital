package edu.istdab.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import edu.istdab.dto.ConsultaListaExamenDTO;
import edu.istdab.exception.ModeloNotFoudException;
import edu.istdab.model.Consulta;
import edu.istdab.service.IConsultaService;

@RestController
@RequestMapping("/consultas")
public class ConsultaController {

	@Autowired
	private IConsultaService service;
	
	@GetMapping
	public ResponseEntity<List<Consulta>> listar(){
		List<Consulta> lista= service.listar();
		return new ResponseEntity<List<Consulta>>(lista,HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Consulta> listarPorId(@PathVariable ("id") Integer id){
		Consulta consulta = service.leerPorId(id);
		if (consulta.getIdConsulta()==null) {
			throw new ModeloNotFoudException("ID no encontrado "+id);
		}
		return new ResponseEntity<Consulta>(consulta,HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<Consulta> registrar (@RequestBody ConsultaListaExamenDTO dto){
		Consulta con= service.registrarTransaccional(dto);
		URI location =ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(con.getIdConsulta()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<Consulta> modificar(@RequestBody Consulta consulta){
		Consulta con= service.modificar(consulta);
		return new ResponseEntity<Consulta>(con,HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> eliminar(@PathVariable("id") Integer id)
	{
		Consulta con =  service.leerPorId(id);
		if (con.getIdConsulta()==null) {
			throw new ModeloNotFoudException("ID no encontrado "+id);
		}
		service.eliminar(id);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
}
