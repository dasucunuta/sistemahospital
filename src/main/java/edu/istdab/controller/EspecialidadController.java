package edu.istdab.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import edu.istdab.exception.ModeloNotFoudException;
import edu.istdab.model.Especialidad;
import edu.istdab.service.IEspecialidadService;

@RestController
@RequestMapping("/especialidades")
public class EspecialidadController {

	@Autowired
	private IEspecialidadService service;
	
	@GetMapping
	public ResponseEntity<List<Especialidad>> listar() {
		List<Especialidad> lista= service.listar();
		return new ResponseEntity<List<Especialidad>>(lista,HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Especialidad> listarPorId(@PathVariable("id") Integer id)
	{
		Especialidad esp= service.leerPorId(id);
		if (esp.getIdEspecialidad()==null) {
			throw new ModeloNotFoudException("ID no encontrado "+id);
		}
		return new ResponseEntity<Especialidad>(esp, HttpStatus.OK); 
	}
	
	@PostMapping
	public ResponseEntity<Especialidad> registrar(@RequestBody Especialidad especialidad)
	{
		Especialidad esp =  service.registrar(especialidad);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(esp.getIdEspecialidad()).toUri();
		return ResponseEntity.created(location).build(); 
	}
	@PutMapping
	public ResponseEntity<Especialidad> modificar(@Valid @RequestBody Especialidad especialidad)
	{
		Especialidad esp =  service.registrar(especialidad);
		return new ResponseEntity<Especialidad>(esp, HttpStatus.OK); 
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> eliminar(@PathVariable("id") Integer id)
	{
		Especialidad esp =  service.leerPorId(id);
		if (esp.getIdEspecialidad()==null) {
			throw new ModeloNotFoudException("ID no encontrado "+id);
		}
		service.eliminar(id);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
	
}
