package edu.istdab.controller;

import java.net.URI;
import java.util.List;

import javax.servlet.Servlet;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import edu.istdab.exception.ModeloNotFoudException;
import edu.istdab.model.Paciente;
import edu.istdab.service.IPacienteService;

@RestController
@RequestMapping("/pacientes")
public class PacienteController {
	
	@Autowired
	public IPacienteService service;
	
	@GetMapping
	public ResponseEntity<List<Paciente>> listar() {
		List<Paciente> lista= service.listar();
		return new ResponseEntity<List<Paciente>>(lista,HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Paciente> listarPorId(@PathVariable("id") Integer id) {
		Paciente paciente =  service.leerPorId(id);
		if (paciente.getIdPersona()==null) {
			throw new ModeloNotFoudException("ID no encontrado "+id);
		}
		return new ResponseEntity<Paciente>(paciente, HttpStatus.OK); 
	}
	
	@PostMapping
	public ResponseEntity<Paciente> registrar(@Valid @RequestBody Paciente paciente)
	{
		Paciente pac =  service.registrar(paciente);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(pac.getIdPersona()).toUri();
		return ResponseEntity.created(location).build(); 
	}
	
	@PutMapping
	public ResponseEntity<Paciente> modificar(@Valid @RequestBody Paciente paciente)
	{
		Paciente pac =  service.registrar(paciente);
		return new ResponseEntity<Paciente>(pac, HttpStatus.OK); 
	}
	  
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> eliminar(@PathVariable("id") Integer id)
	{
		Paciente paciente =  service.leerPorId(id);
		if (paciente.getIdPersona()==null) {
			throw new ModeloNotFoudException("ID no encontrado "+id);
		}
		service.eliminar(id);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
}
