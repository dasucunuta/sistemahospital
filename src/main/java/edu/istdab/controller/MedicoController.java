package edu.istdab.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import edu.istdab.exception.ModeloNotFoudException;
import edu.istdab.model.Medico;
import edu.istdab.service.IMedicoService;

@RestController
@RequestMapping("/medicos")
public class MedicoController {

	@Autowired
	private IMedicoService service;
	
	@GetMapping
	public ResponseEntity<List<Medico>> listar() {
		List<Medico> lista= service.listar();
		return new ResponseEntity<List<Medico>>(lista,HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Medico> listarPorId(@PathVariable("id") Integer id)
	{
		Medico medico= service.leerPorId(id);
		if (medico.getIdPersona()==null) {
			throw new ModeloNotFoudException("ID no encontrado "+id);
		}
		return new ResponseEntity<Medico>(medico, HttpStatus.OK); 
	}
	
	@PostMapping
	public ResponseEntity<Medico> registrar(@RequestBody Medico medico)
	{
		Medico med =  service.registrar(medico);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(med.getIdPersona()).toUri();
		return ResponseEntity.created(location).build(); 
	}
	@PutMapping
	public ResponseEntity<Medico> modificar(@Valid @RequestBody Medico medico)
	{
		Medico medic =  service.registrar(medico);
		return new ResponseEntity<Medico>(medic, HttpStatus.OK); 
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> eliminar(@PathVariable("id") Integer id)
	{
		Medico medic =  service.leerPorId(id);
		if (medic.getIdPersona()==null) {
			throw new ModeloNotFoudException("ID no encontrado "+id);
		}
		service.eliminar(id);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
	
}
